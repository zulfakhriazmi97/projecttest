<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
// use Illuminate\Support\Facades\DB;

class CreateProcedure extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         $procedure = "
            CREATE PROCEDURE proc_insert_user 
                @uName varchar(255), 
                @uEmail varchar(255), 
                @uPassword varchar(255), 
                @phone varchar(255), 
                @address varchar(255)
            AS
            BEGIN
            INSERT INTO users (name, email, password, phone, address, created_at) VALUES (@uName, @uEmail, @uPassword, @phone, @address, GETDATE());
            END
            ";
        \DB::unprepared($procedure);
    }

    public function down()
    {
        DB::unprepared('DROP PROCEDURE proc_insert_user');
    }
}
