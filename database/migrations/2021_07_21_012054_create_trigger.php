<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrigger extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared("
        CREATE TRIGGER tr_User_Activity_Log ON users AFTER INSERT
            AS
            BEGIN
                INSERT INTO activity_logs (activity, created_at, updated_at) 
                VALUES ('Menambah data user', GETDATE(), null);
        END
        ");
    }

    public function down()
    {
        DB::unprepared('DROP TRIGGER tr_User_Activity_Log');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
}
