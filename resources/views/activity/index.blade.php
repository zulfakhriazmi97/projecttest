@extends('layouts.app')

@section('content')
<!-- Page Title Header Starts-->
<div class="row page-title-header">
    <div class="col-12">
    <div class="page-header">
        <h4 class="page-title">Log Aktifitas</h4>
        <div class="quick-link-wrapper w-100 d-md-flex flex-md-wrap">
            <ul class="quick-links ml-auto">
                <li><a href="#">Log Aktifitas</a></li>
            </ul>
        </div>
    </div>
    </div>
</div>
<!-- Page Title Header Ends-->
<div class="row">
    <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Data Log Aktifitas</h4>
                <hr>
                <div class="card-description">
                    <div class="float-left">
                        <a href="{{ url('activity_log') }}" class="btn btn-secondary btnReload" title="Refresh"><i class="mdi mdi-refresh"></i></a>
                    </div>
                    <div class="table-responsive">
                        @if (session('status'))
                            <div class="alert alert-primary alert-dismissible">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                                {{ session('status') }}
                            </div>
                        @endif 
                        <hr>
                        <table class="table table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Deskripsi</th>
                                    <th class="hidden-sm-down" width="80px">Ditambahkan</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($datas as $key => $data)
                                <tr>
                                    <td>{{ $datas->firstItem() + $key }}</td>
                                    <td>{{ $data->activity }}</td>
                                    <td class="hidden-sm-down">
                                        <span>{{ date('H:i:s', strtotime($data->created_at)) }},</span><br>
                                        <span>{{ date('d-M-Y', strtotime($data->created_at)) }}</span>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="card-footer text-right">
                {{ $datas->appends(Request::all())->links() }}
            </div>
        </div>
    </div>
</div>
@endsection