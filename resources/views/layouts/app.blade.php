<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name') }}</title>

    <!-- plugins:css -->
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous"> -->
    <link rel="stylesheet" href="{{ asset('theme/vendors/iconfonts/mdi/css/materialdesignicons.min.css') }}">
    <link rel="stylesheet" href="{{ asset('theme/vendors/iconfonts/ionicons/css/ionicons.css') }}">
    <link rel="stylesheet" href="{{ asset('theme/vendors/iconfonts/typicons/src/font/typicons.css') }}">
    <link rel="stylesheet" href="{{ asset('theme/vendors/iconfonts/flag-icon-css/css/flag-icon.min.css') }}">
    <link rel="stylesheet" href="{{ asset('theme/vendors/css/vendor.bundle.base.css') }}">
    <link rel="stylesheet" href="{{ asset('theme/vendors/css/vendor.bundle.addons.css') }}">
    <link rel="stylesheet" href="{{ asset('theme/vendors/spinner/jquery-spinner.min.css') }}">
    <!-- endinject -->
    <!-- plugin css for this page -->
    <link rel="stylesheet" href="{{ asset('theme/vendors/iconfonts/font-awesome/css/font-awesome.min.css') }}" />
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="{{ asset('theme/css/shared/style.css') }}">
    <!-- endinject -->
    <!-- Layout styles -->
    <link rel="stylesheet" href="{{ asset('theme/css/demo_1/style.css') }}">
    
    <link rel="stylesheet" href="{{ asset('theme/css/custom.css') }}">
    <style>
        @media print
        {    
            .no-print, .no-print * {
                display: none !important;
            }

            button, a {
                display: none !important;
            }
        }
    </style>
    @yield('head-extra')
</head>
<body id="mainContainer">

    <div class="container-scroller">
      <!-- partial:partials/_navbar.html -->
      <nav class="navbar default-layout col-lg-12 col-12 p-0 fixed-top d-flex flex-row no-print">
        <div class="text-center navbar-brand-wrapper d-flex align-items-top justify-content-center">
            <a class="navbar-brand brand-logo" style="color:#fff !important" href="/">
                {{ config('app.name') }}
                <!-- <img src="{{ asset('hospicloud-logo-white.png') }}" style="width: 160px;" alt="logo"> -->
            </a>
            <a class="navbar-brand brand-logo-mini" href="/">
                <img src="{{ asset('favicon.png') }}" style="width: 40px;" alt="logo">
            </a>
        </div>

      </nav>
      <!-- partial -->
      <div class="container-fluid page-body-wrapper">
        <!-- partial:partials/_sidebar.html -->
        <nav class="sidebar sidebar-offcanvas no-print" id="sidebar">
            <ul class="nav">
                <li class="nav-item nav-profile">
                    <a href="#" class="nav-link" style="height: auto;">
                        <div class="profile-image">
                            <i class="fa fa-2x fa-user-circle"></i>
                        </div>
                        <div class="text-wrapper">
                            <p class="profile-name">Welcome</p>
                            <p class="designation text-xs"><span class="d-block mb-1">Selamat Datang</small></p>
                        </div>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ url('home') }}">
                        <i class="menu-icon typcn typcn-document-text"></i>
                        <span class="menu-title">Dashboard</span>
                    </a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="{{ url('users') }}">
                        <i class="menu-icon typcn typcn-shopping-bag"></i>
                        <span class="menu-title">User</span>
                    </a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="{{ url('activity_log') }}">
                        <i class="menu-icon typcn typcn-shopping-bag"></i>
                        <span class="menu-title">Log Aktifitas</span>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- partial -->
        <div class="main-panel">
            <div class="content-wrapper">
                @yield('content')
            </div>
            <!-- content-wrapper ends -->
            <!-- partial:partials/_footer.html -->
            <footer class="footer">
                <div class="container-fluid clearfix">
                    <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">{{config('app.name')}}</span>
                    <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center"><i>Zulfakhri | 2021 </i> 
                    </span>
                </div>
            </footer>
            <!-- partial -->
        </div>
        <!-- main-panel ends -->
        </div>
        <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->
    <!-- plugins:js -->
    <script src="{{ asset('theme/vendors/js/vendor.bundle.base.js') }}"></script>
    <script src="{{ asset('theme/vendors/js/vendor.bundle.addons.js') }}"></script>
    <script src="{{ asset('theme/vendors/spinner/jquery-spinner.min.js') }}"></script>
    <!-- endinject -->
    <!-- inject:js -->
    <script src="{{ asset('theme/js/shared/off-canvas.js') }}"></script>
    <script src="{{ asset('theme/js/shared/misc.js') }}"></script>
    <!-- endinject -->
    <script src="{{ asset('theme/js/demo_1/dashboard.js') }}"></script>
    <script src="{{ asset('js/custom.js') }}"></script>
    <!-- End custom js for this page-->
    <script type="text/javascript">
        const spinner = new jQuerySpinner({ parentId: 'mainContainer' });
        spinner.show();
        $(document).ready(function() { spinner.hide(); });
    </script>
    @yield('end-script')
</body>
</html>
