@extends('layouts.app')

@section('content')
<!-- Page Title Header Starts-->
<div class="row page-title-header">
    <div class="col-12">
    <div class="page-header">
        <h4 class="page-title">User</h4>
        <div class="quick-link-wrapper w-100 d-md-flex flex-md-wrap">
            <ul class="quick-links ml-auto">
                <li><a href="#">User</a></li>
                <li><a href="#">Form</a></li>
            </ul>
        </div>
    </div>
    </div>
</div>
<!-- Page Title Header Ends-->
<div class="row">
    <div class="col-md-12 d-flex align-items-stretch grid-margin">
        <div class="row flex-grow">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Form @if(!empty($user)){{ 'Edit' }}@else{{ 'Tambah' }}@endif Data</h4>
                        <hr>
                        <form class="forms-sample" enctype="multipart/form-data" action="@if(!empty($user)) {{ route('users.update', [$user->uuid]) }} @else {{ route('users.store') }} @endif" method="POST">
                            @csrf
                            @if(!empty($user))
                                <input type="hidden" value="PUT" name="_method">
                            @endif
                            <div class="row">
                                @if (session('status'))
                                <div class="col-md-12">
                                    <div class="alert alert-warning">
                                        {{ session('status') }}
                                    </div>
                                </div>
                                @endif 
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="name">Nama</label>
                                        <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" id="name" placeholder="Nama Lengkap" value="@if(!empty($user)){{ $user->name }}@else{{ old('name') }}@endif">
                                        @error('name')<div class="invalid-feedback">* {{ $message }}</div>@enderror
                                    </div>
                                    
                                    <div class="form-group">
                                        <label for="phone">Telp</label>
                                        <input type="text" class="form-control @error('phone') is-invalid @enderror" name="phone" id="phone" placeholder="Telp" value="@if(!empty($user)){{ $user->phone }}@else{{ old('phone') }}@endif">
                                        @error('phone')<div class="invalid-feedback">* {{ $message }}</div>@enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="address">Alamat</label>
                                        <input type="text" class="form-control @error('address') is-invalid @enderror" name="address" id="address" placeholder="Alamat" value="@if(!empty($user)){{ $user->address }}@else{{ old('address') }}@endif">
                                        @error('address')<div class="invalid-feedback">* {{ $message }}</div>@enderror
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="email">Email</label>
                                        <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" id="email" placeholder="Email" value="@if(!empty($user)){{ $user->email }}@else{{ old('email') }}@endif">
                                        @error('email')<div class="invalid-feedback">* {{ $message }}</div>@enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="password">Password</label>
                                        <input type="password" class="form-control @error('password') is-invalid @enderror" name="password" id="password" placeholder="Password" >
                                        @error('password')<div class="invalid-feedback">* {{ $message }}</div>@enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="password_confirmation">Konfirmasi Password</label>
                                        <input type="password" class="form-control @error('password_confirmation') is-invalid @enderror" name="password_confirmation" id="password_confirmation" placeholder="Konfirmasi Password" >
                                        @error('password_confirmation')<div class="invalid-feedback">* {{ $message }}</div>@enderror
                                    </div>
                                </div>
                            </div>
                            <a href="{{ url('users') }}" class="btn btn-danger">Batal</a>
                            <button type="submit" class="btn btn-success float-right">Simpan</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection