@extends('layouts.app')

@section('content')
<!-- Page Title Header Starts-->
<div class="row page-title-header">
    <div class="col-12">
    <div class="page-header">
        <h4 class="page-title">User</h4>
        <div class="quick-link-wrapper w-100 d-md-flex flex-md-wrap">
            <ul class="quick-links ml-auto">
                <li><a href="#">User</a></li>
            </ul>
        </div>
    </div>
    </div>
</div>
<!-- Page Title Header Ends-->
<div class="row">
    <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Data User</h4>
                <hr>
                <div class="card-description">
                    <div class="float-left">
                        <a href="{{ url('users') }}" class="btn btn-secondary btnReload" title="Refresh"><i class="mdi mdi-refresh"></i></a>
                        <a href="{{ url('users/create') }}" class="btn btn-primary btn-fw"><i class="mdi mdi-plus-outline text-white"></i> Tambah Data</a>
                    </div>
                    <div class="float-right">
                        <form method="GET" action="{{ url('users') }}">
                            <div class="form-group">
                                <div class="input-group">
                                    <input type="text" name="filter" id="filter" value="{{ Request::get('filter') }}" class="form-control" placeholder="Pencarian" aria-label="Pencarian" aria-describedby="colored-addon3">
                                    <div class="input-group-append bg-primary border-primary">
                                        <button type="submit" class="input-group-text bg-transparent"><i class="mdi mdi-magnify text-white"></i></button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="table-responsive">
                        @if (session('status'))
                            <div class="alert alert-primary alert-dismissible">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                                {{ session('status') }}
                            </div>
                        @endif 
                        <table class="table table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Nama</th>
                                    <th class="hidden-sm-down">Info</th>
                                    <th class="hidden-sm-down" width="80px">Ditambahkan</th>
                                    <th class="btnAction">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($datas as $key => $data)
                                <tr>
                                    <td>{{ $datas->firstItem() + $key }}</td>
                                    <td>{{ $data->name }}</td>
                                    <td class="hidden-sm-down">
                                        <small>
                                            <b>Telp : </b> {{ $data->phone ?? '-' }}<br>
                                            <b>Email : </b> {{ $data->email ?? '-' }}<br>
                                            <b>Alamat : </b> {{ $data->address ?? '-' }}
                                        </small>
                                    </td>
                                    <td class="hidden-sm-down">
                                        <span>{{ date('H:i:s', strtotime($data->created_at)) }},</span><br>
                                        <span>{{ date('d-M-Y', strtotime($data->created_at)) }}</span>
                                    </td>
                                    <td>
                                        <div class="dropdown">
                                            <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Aksi </button>
                                            <div class="dropdown-menu" x-placement="bottom-start">
                                                <a class="dropdown-item" href="{{ route('users.edit', [$data->id]) }}">Edit</a>
                                                <div class="dropdown-divider"></div>
                                                <a class="dropdown-item" href="#" onclick="event.preventDefault(); document.getElementById('delete-form-{{ $data->id }}').submit();">Hapus</a>
                                            </div>
                                        </div>
                                        @if($data->id != 1)
                                        <form id="delete-form-{{ $data->id }}" onsubmit="return confirm('Delete data?')" class="d-inline invisible" action="{{ route('users.destroy', [$data->id]) }}" method="POST">
                                            @csrf
                                            <input type="hidden" name="_method" value="DELETE">
                                            <button type="submit" class="btn btn-danger text-white btn-sm invisible" title="Hapus"><i class="fas fa-trash"></i></button>
                                        </form>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="card-footer text-right">
                {{ $datas->appends(Request::all())->links() }}
            </div>
        </div>
    </div>
</div>
@endsection