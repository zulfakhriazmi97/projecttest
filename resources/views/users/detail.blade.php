@extends('layouts.app')

@section('content')
<!-- Page Title Header Starts-->
<div class="row page-title-header">
    <div class="col-12">
    <div class="page-header">
        <h4 class="page-title">User</h4>
        <div class="quick-link-wrapper w-100 d-md-flex flex-md-wrap">
            <ul class="quick-links ml-auto">
                <li><a href="#">Master Data</a></li>
                <li><a href="#">User Akses</a></li>
                <li><a href="#">Detail</a></li>
            </ul>
        </div>
    </div>
    </div>
</div>
<!-- Page Title Header Ends-->
<div class="row">
    <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Detail User Akses</h4>
                <hr>
                <div class="card-description">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <b for="name">Nama : </b>
                                <p>{{ $user->name ?? '-' }}</p>
                            </div>
                            <div class="form-group">
                                <b for="username">Username : </b>
                                <p>{{ $user->username ?? '-' }}</p>
                            </div>
                            <div class="form-group">
                                <b for="email">Email : </b>
                                <p>{{ $user->email ?? '-' }}</p>
                            </div>
                            <div class="form-group">
                                <b for="phone">Telp : </b>
                                <p>{{ $user->phone ?? '-' }}</p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <b for="address">Alamat : </b>
                                <p>{{ $user->address ?? '-' }}</p>
                            </div>
                            <div class="form-group">
                                <b for="roles">Hak Akses : </b>
                                <p>@foreach ($user->roles as $k => $role){{ $role->name }},@endforeach</p>
                            </div>
                            <div class="form-group">
                                <b for="status">status : </b>
                                @if ($user->status == 0)
                                    <p><label class="badge badge-success">Aktif</label></p>
                                @elseif ($user->status == 1)
                                    <p><label class="badge badge-warning">NonAktif</label></p>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-12">
                            <br>
                            <a href="{{ url('masters/users') }}" class="btn btn-primary btn-fw"><i class="mdi mdi-arrow-left text-white"></i> Kembali</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection