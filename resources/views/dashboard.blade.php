@extends('layouts.app')

@section('content')
<!-- Page Title Header Starts-->
<div class="row page-title-header">
    <div class="col-12">
    <div class="page-header">
        <h4 class="page-title">Dashboard</h4>
        <div class="quick-link-wrapper w-100 d-md-flex flex-md-wrap">
            <ul class="quick-links ml-auto">
                <li><a href="#">Dashboard</a></li>
            </ul>
        </div>
    </div>
    </div>
</div>
<!-- Page Title Header Ends-->
<div class="row">
    <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <div class="card-description">
                    <h2>Selamat Datang</h2>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection