<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\User;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filter = $request->get('filter');

        $data = User::orderBy('id', 'desc');

        if ($request->has('filter')){
            if (!empty($filter)) {
                $data->where('name', 'like', '%'.$filter.'%');
                $data->orWhere('username', 'like', '%'.$filter.'%');
                $data->orWhere('email', 'like', '%'.$filter.'%');
                $data->orWhere('phone', 'like', '%'.$filter.'%');
                $data->orWhere('address', 'like', '%'.$filter.'%');
            }
        }

        $datas = $data->paginate(10);
        
        return view('users.index', compact('datas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('users.form');
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'password' => 'required|confirmed|min:4',
        ],[
            'password.confirmed' => 'Password dan konfirmasi password tidak sesuai',
        ]);

        DB::beginTransaction();

        try {
            $values = array(
                $request->input('name'),
                $request->input('email'),
                \Hash::make($request->input('password')),
                $request->input('phone'),
                $request->input('address'),
            );
            DB::insert('EXEC proc_insert_user ?, ?, ?, ?, ?', $values);
               

            DB::commit();

            return redirect()->route('users.index')->with('status', 'Successfully created');
        } catch (\Throwable $th) {
            throw $th;

            DB::rollback();
            return redirect()->route('users.create')->with('status', 'FAILED');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function pengguna()
    {
        $user = User::orderBy('id', 'desc')->get();

        return response()->json($user);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
