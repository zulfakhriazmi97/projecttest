<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\ActivityLog;

class ActivityLogController extends Controller
{
     public function index(Request $request)
    {
        $filter = $request->get('filter');

        $data = ActivityLog::orderBy('created_at', 'desc');
        $datas = $data->paginate(10);
        
        return view('activity.index', compact('datas'));
    }
}
